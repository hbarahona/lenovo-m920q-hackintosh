# Lenovo m920q Hackintosh

![m920q](m920q.png)

# Lenovo Thinkcentre Tiny m920q
- Model: Lenovo Thinkcentre Tiny m920q
- Catalina 10.15.6
- OpenCore x.x
  - ...
  - ... WIP ...

## System Specifications:
- CPU: Intel i9-9900T
- GPU: Intel UHD Graphics 630
- RAM: Kingston HyperX  3200MHz 16GBx2 _(running at 2666MHz)_
- Storage: NVMe WD Black SN750 1TB 
- Motherboard: OEM SSF. Chipset Intel Q370
- Audio: ALC233VB2 _(it seems to be actually ALC235)_
- Ethernet: Intel I219-LM
- Wifi/BT: Intel Wifi 6 AX200, Bluetooth 5.1
- BIOS M1UKT56A (01/12/2020)

## What works:
- Front panel: Microphone, combo audio jack, usb-c. 
- Rear panel:
 - Display Port at 75hz with audio output.
 - All USB and Ethernet port.
- Hardware acceleration, iGPU
- Wake up from sleep
- Internal speaker 1.5w
- Front usb-c recognizes the Lenovo usb-C Mini Dock, works perfectly all ports _(HDMI, USB, combo audio jack and Ethernet port)_

## Work in Progress:
- Wifi compatibility with [itlwm](https://openintelwireless.github.io/) and HeliPort
- HDMI output
- Waiting for thunderbolt module _(will it be possible to use an eGPU?)_
